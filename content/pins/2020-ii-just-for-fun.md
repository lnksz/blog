---
title: Just for Fun - Linus Torvalds
date: 2020-07-11
subtitle: A glimpse into the life of a living legend
tags: ["book", "legend"]
---

[![Just For Fun](https://i.harperapps.com/covers/9780066620732/x300.jpg)](https://www.harpercollins.com/9780066620732/just-for-fun/)

Feels most of the time as reading a the transcript of a banter between friends.

> You’d drag me (with family) camping and (without family) skydiving. **Things that I wouldn’t ever do otherwise, just because I think I’m too busy.** Give me an excuse to do the things I haven’t done during the last three years **even though all the opportunities are there…**

- __[book's website](https://www.harpercollins.com/9780066620732/just-for-fun/)__
