---
title: The Pragmatic Programmer (Anniversary Edition)
date: 2020-03-04
subtitle: If I'd have read it 5 years before...
tags: ["book", "prog"]
---

[![The Pragmatic Programmer](https://pragprog.com/titles/tpp20/tpp20_hu7d9a813cf46675dfb7b1bf4930cee733_2802494_250x0_resize_q75_box.jpg)](https://pragprog.com/book/tpp20/the-pragmatic-programmer-20th-anniversary-edition)

The text is easily readable, well illustrated with parabola and everyday experiences. Some of the concise "lessons" resonated with me, giving me some big "aha moments". There were some suggestions which I already recognized in my everyday life and a load of ideas which I will try to put into practice.

> "you can always fall back on the ultimate “easy to change” path: try to make what you write replaceable. That way, whatever happens in the future, this chunk of code won’t be a roadblock..."

> "We routinely set compiler warning levels as high as possible. It doesn’t make sense to waste time trying to find a problem that the computer could find for you! We need to concentrate on the harder problems at hand."

- I was already backing up some of my configuration files manually, but the book gave me a push to do this systematically. (And proven to be great during a migration from one Linux distribution to an other.) 
- I was already a 10-finger typist, but the relevant chapter gave me a new motivation to find "keyboard-able" routine tasks, where I still grabed for the mouse. Turns out there was quite a few cases where I just defaulted to the mouse, but by setting up some shortcuts my day-to-day experience improved a lot. (E.g. inter-pane navigation in VS Code.) It also motivated me to go back to (neo-) vim, to at least get the workflow into my muscle-memory. 
- __[book's website](https://pragprog.com/book/tpp20/the-pragmatic-programmer-20th-anniversary-edition)__
