---
title: The Practice of Programming
date: 2019-10-10
subtitle: First "real" programming book
tags: ["book", "prog", "C"]
---

[![The Practice of Programming](https://www.cs.princeton.edu/~bwk/tpop.webpage/tpop.jpg)](https://www.cs.princeton.edu/~bwk/tpop.webpage/)

First "programming" book after my electrical engineering curriculum which helped me transition from "university style" coding to "industrial style" coding.   

- Next to the time-less advices/introductions, I could learn some C tricks, which I could put to use directly in my work. 
(At the time I was working on an embedded C project.) Like the use of `enum` blocks for constants instead of using the wide-spread `#define` macros.
- **TODO** find and upload my analog notes/bookmarks from the book.
- __[book's website](https://www.cs.princeton.edu/~bwk/tpop.webpage/)__
