---
title: Kernel Cmdline Dilemma
date: 2020-07-26
subtitle: all ways lead to Rome
tags: ["blog", "prog", "kernel", "bootstrap"]
---

Some weeks ago I had to abruptly take over the maintenance of our "pre user-space stack" for the embedded Linux board we have been developing. This includes:
- a 2nd level bootstraploader [at91bootstrap](https://github.com/linux4sam/at91bootstrap)
- the Linux Kernel
- the [device tree…](https://www.kernel.org/doc/Documentation/devicetree/usage-model.txt#:~:text=The%20%22Open%20Firmware%20Device%20Tree,code%20details%20of%20the%20machine.)
- root file System via [buildroot](https://buildroot.org/)

So far, the most memorable "clean-up" has been the definition of the RAM's size. (Note, we are talking about an embedded system with fix installed memories)

The discovery that the source of our bug(s) has been the misconfiguration of the kernels perception of the RAM size was ending a longer debugging session.

But during the attempt to fix the issue I have seen the multitude of places to define the memory size for the kernel. Here an overview:

- `bootstrap/Kconfig/CONFIG_CMDLINE`
- `bootstrap/contrib/xxx/board/Config.Linuxargs (or something similar)`
- `bootstrap/Code/fdt/fixup-{chosen,mem}-node`
- `kernel/arch/boot/dts/board.dts/memory-node`
- `kernel/arch/boot/dts/board.dts/chosen-node`
- `kernel/Kconfig/boot-options/force-compiled-cmdline`

Especially,  the kernel's 'forced' configuration was a new   option for me.

Still not sure, what the best way is.
On the one hand, the forced option feels the most security concious, because then even with a manipulated bootstrap, one couldn't modify the kernel's behavior via cmdline.
On the other hand, the DTS describes already the HW, and in case of an embedded (static) configuration, this parameter feels to be needed there.

But the bootstrap also needs this configuration, as it is responsible for the RAM initialization.

So what I ended up doing:
- Config in bootstrap is the single source of truth.
- Bootstrap fixes up the `chosen` and the `memory` nodes in the DTB, and doesn't pass `mem=` via cmdline
- Kernel doesn't force its compiled cmdline, but accepts the one offered by the bootstrap (we have a secure boot strategy, where the bootstrap was considered trustworthy.)

This has the drawback that if the memory and chosen nodes are present in the DTS, then the developer may see different values than the actual fixed-up version passed to the kernel.

Notes:
- `/proc/cmdline` or bootlog
- `/proc/device-tree/chosen/`




